import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  nForm: FormGroup;
  post: any;
  firstName : string = '';
  lastName : string = '';
  creditCard : any = [];
  titleAlert : string = 'This is required'

  constructor(private fb: FormBuilder) {
    
    this.nForm = fb.group({
      'firstName': [null,Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(100)])],
      'lastName' : [null, Validators.required],
      'creditCard' : [null, Validators.required],
      'validate' : ''
  })
}
  addPost(post){
    this.firstName = post.firstname;
    this.lastName = post.lastname;
  }

}
